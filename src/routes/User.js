const express = require('express');
const Register = require('../models/User')
const bcrypt = require('bcryptjs');
const auth = require('../middleware/auth')
const multer = require('multer');
const sharp = require('sharp');
require("../db/conn");

const router = express.Router();

router.get('/',(req, res) => {
    res.render('index');
});

router.get('/register',(req, res)=>{
    res.render("register");
});

router.post('/register',async (req, res)=>{

    const {fullName,email,gender,password,confirmPassword} = req.body

    try{
        // const password = req.body.password;
        // const cpassword = req.body.confirmPassword;
        const userExist = await Register.findOne({email: email})

        if(userExist){
            return res.status(422).render("register",{userExistError:"User Already Exists! Go to Login."});
        }

        if (password===confirmPassword){

            const studentRegister = new Register(req.body)
            // const studentRegister = new Register({
            //     fullName: fullName, //req.body.fullName
            //     email: email,
            //     gender: gender,
            //     password: password,
            //     confirmPassword: confirmPassword
            // });
            const registered = await studentRegister.save();
            const token = await studentRegister.generateAuthToken()
            res.cookie("jwt",token,{
                // expires: new Date(Date.now()+3000*5),
                // httpOnly: true
            });
            res.status(201).redirect('/profilepic');

        }else{
            res.status(500).render('register',{
                err1: 'Password Did not Match'
            });
        }

    }catch(e){
        res.status(400).render('register',{
            err2:e.message
        });
    }
})

router.get('/login', (req, res)=>{
    res.render('login');
})

router.post('/login', async (req, res)=>{
    try {
        const email = req.body.email;
        const password = req.body.password;

        const userEmail = await Register.findOne({ email: email});
        if(!userEmail){

            res.status(404).render('login',{
                noUserError: 'User Does not Exist! Would You Like To Register',
            })

        }

        const isMatch = await bcrypt.compare(password,userEmail.password);

        const token = await userEmail.generateAuthToken()
        //console.log(token);
        res.cookie("jwt",token,{
                // expires: new Date(Date.now()+3000*10),
                // httpOnly: true
        });

        if(isMatch){
            res.status(201).redirect('/contents');
        }else{
            res.status(404).render("login",{
                err1: "Invalid Credentials"
            });
        }
    }catch(e){
        res.status(400).render("login",{
            err2:e.message
        });
    }

})


router.get("/contents",auth,async (req, res)=>{
    //const user = await Register.find({})
    // console.log("req body " + req.user._id);
    // console.log(req.user.fullName);
    // console.log(req.user.email);
    
    console.log(typeof req.user.avatar);
    res.status(200).render('contents',{
        title:"You will loose",
        userName:req.user.fullName,
        userLastName:req.user.lastName,
        email:req.user.email,
        gender: req.user.gender,
        id: req.user._id,
        avatar:req.user.avatar
    })
})

router.get("/contents/delete",auth, async (req, res) => {
    try {
        await req.user.remove();
        console.log("User deleted");
        res.status(200).redirect("/");
    } catch (e) {
        res.status(500).render('contents',{
            err1: e.message
        })
    }
})

router.get("/update",auth, async (req, res)=>{
    res.status(200).render('update',{
        userFirstName:req.user.fullName,
        userLastName:req.user.lastName,
        email:req.user.email,
        gender:req.user.gender,
    })
})



router.post("/update",auth, async (req, res)=>{

    const updates = Object.keys(req.body);
    const allowUpdates = ["fullName","lastName","email","gender"];
    const isValidOperation = updates.every((update)=>allowUpdates.includes(update));
    if(!isValidOperation){
        return res.status(400).render('update',{
            err:"Invalid Update"
        })
    }
    try {
        updates.forEach((update)=> req.user[update]=req.body[update])
        await req.user.save();
        res.status(200).redirect("/contents");
    } catch (error) {
        res.status(400).render('update',{
            err:error.message
        })
    }
})


router.get("/update/password",auth, async(req,res)=>{
    try {
        res.status(200).render('updatePass')
    } catch (e) {
        res.status(500).render('updatePass',{error:e.message})
    }
})

router.post("/update/password",auth, async(req,res)=>{
    const updates = Object.keys(req.body);
    const allowUpdates = ["password","confirmPassword"];
    const isValidOperation = updates.every((update) => allowUpdates.includes(update));
    if(!isValidOperation){
        return res.status(400).render('updatePass',{error:"Invalid Updates."})
    }
    try {
        updates.forEach((update) => req.user[update]=req.body[update])
        await req.user.save();
        res.status(200).redirect("/contents");
    } catch (e) {
        res.status(500).render('updatePass',{error:e.message})
    }
})



router.get('/logout',auth,async (req, res)=>{
    try{
        req.user.tokens = req.user.tokens.filter((currElement)=>{
            return currElement.token !== req.token
        })
        res.clearCookie('jwt');
        await req.user.save()
        res.redirect('/');
    }catch(e){
        res.send(e.message);
    }
})

router.get('/logoutAll',auth, async (req, res)=>{
    try {
        req.user.tokens = [];
        res.clearCookie('jwt');
        await req.user.save();
        res.redirect('/');
    } catch (e) {
        res.render('index',{
            err:e.message
        })
    }
})

router.get('/profilepic',auth,async (req, res)=>{
    try {
        res.status(200).render('profilepic');
    } catch (error) {
        res.status(500).render('profilepic',{err:error.message})
    }
})

const upload = multer({
    limits:{
        filesize: 1000000
    },
    fileFilter(req,file,cb){
        if(!file.originalname.match(/\.(jpg|jpeg|png)$/)){
            cb(new Error('Please Upload a Photo'));
        }
        cb(undefined,true);
    }
})

router.post('/profilepic',auth,upload.single('avatar') ,async (req, res)=>{
    try {
        const buffer = await sharp(req.file.buffer).resize({width:250,height:250}).png().toBuffer();
        req.user.avatar = buffer
        await req.user.save();
        res.status(200).redirect('/contents');
    } catch (error) {
        res.status(500).render('profilepic',{
            err:error.message
        })
    }
},(err,req,res,next) => {
    res.status(400).render('profilepic',{
        err:err.message
    })
})

router.get('/users/:id/avatar', async(req, res) => {
    try {
        const user = await Register.findById(req.params.id)

        if (!user || !user.avatar) {
            throw new Error("Cannot Find User or User Image")
        }

        res.set('Content-Type', 'image/png')
        res.status(200).send(user.avatar)
    } catch (e) {
        res.status(404).send(e.message)
    }
})

module.exports = router
