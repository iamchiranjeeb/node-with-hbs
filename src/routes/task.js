const express = require('express');
const MyTasks = require('../models/task');
const auth = require('../middleware/auth')
const router = new express.Router();


router.get('/insertTasks',auth,(req, res) =>{
    res.render('insertTask',{
        ownerName: req.user.fullName
    });
} )

router.post('/insertTasks', auth,async (req, res)=>{
    // const task = new MyTasks({
    //     taskName: req.body.taskName,
    //     isCompleted: req.body.isCompleted,
    // });
    const task = new MyTasks({
        ...req.body,
        owner: req.user._id
    });
    console.log(req.user.fullName + " god is cruel.")

    try {
        await task.save();
        res.status(200).redirect('/allTasks');
    } catch (error) {
        res.status(500).render('insertTask',{
            err:error.message,
        });
    }
})



router.get('/allTasks', auth ,async (req, res) => {
    try {
        const tasks = await MyTasks.find({owner:req.user._id})
        const idf = req.user.fullName
        //console.log("This is " + idf);
        // const tasks2 = await  req.user.populate('tasks').execPopulate()
        // console.log(req.user.tasks)
        if(!tasks){
            return res.status(404).render('allTask',{
                err: "You Do not have any tasks"
            })
        }

        if(tasks.length === 0){
            return res.render('allTask',{
                noTasks : "No Tasks Found, Add Some Tasks"
            })
        }

        res.status(200).render('allTask',{
            // task: req.user.tasks,
            task: tasks
        })
        //console.log(tasks[0].taskName);
        console.log(tasks.length);
    } catch (error) {
        res.status(500).render('allTask',{
            err:error.message
        })
    }
})

router.get('/posts',auth,async(req, res)=>{
    const posts = await MyTasks.find();
    //console.log(req.user.fullName + "cry mc")
    res.status(200).render('posts',{
        post : posts,
        nm: req.user.fullName
    });
    console.log(posts)
})

//update Tasks
router.get('/updateTask/:id', auth, async (req, res)=>{
    const tasks = await MyTasks.findOne({_id:req.params.id,owner:req.user._id})
    console.log(tasks._id)
    res.status(200).render('updateTasks',{
        task:tasks
    });
})

router.post('/updateTask/:id',auth, async (req, res) => {
    const updates = Object.keys(req.body);
    const allowUpdates = ['taskName','memo','isCompleted'];
    const isValidOperation = updates.every((update) => allowUpdates.includes(update))
    if(!isValidOperation){
        return res.status(400).render('updateTasks',{
            err: "Invalid Operation"
        });
    }
    try {
        const myTasks = await MyTasks.findOne({_id:req.params.id,owner:req.user._id});
        if(!myTasks){
            res.status(404).render('updateTasks',{err:"Task Not Found"});
        }
        updates.forEach((update) => myTasks[update] = req.body[update]);
        await myTasks.save();
        res.status(200).redirect('/allTasks');
    } catch (error) {
        res.status(500).render('updateTask',{
            err:error.message
        })
    }
})

//Delete Task
router.get('/deleteTask/:id',auth, async (req, res)=>{
    try {
        const myTasks = await MyTasks.findOneAndDelete({_id:req.params.id,owner:req.user._id});
        if(!myTasks){
            res.status(404).render('updateTasks',{
                err: "Task not found"
            });
        }
        res.status(200).redirect('/allTasks')
    } catch (error) {
        res.status(500).render('updateTasks',{
            err:error.message
        })
    }
})

module.exports = router;