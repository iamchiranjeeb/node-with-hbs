const mongoose = require('mongoose');

const connLink = process.env.DB_CONNECTION

mongoose.connect(connLink,{
    useNewUrlParser:true,
    useUnifiedTopology:true,
    useCreateIndex:true,
}).then(()=>console.log("Connected Successfully ")).catch(e => console.log(`Error ${e.message}`))