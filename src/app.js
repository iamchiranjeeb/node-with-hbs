const express = require('express');
const path = require('path');
const hbs = require('hbs');
const {json} = require('express');
const UserRouter = require('./routes/User');
const taskRouter = require('./routes/task');
const cookieParser = require('cookie-parser');
require("./db/conn");

const app = express();
const PORT = process.env.PORT
const HOST = process.env.HOST
const connPath = path.join(__dirname,'../public');
const templatePath = path.join(__dirname,'../template/views');
const partialPath = path.join(__dirname,'../template/partials');

app.use(express.json());
app.use(cookieParser());
app.use(express.urlencoded({
    extended: false
}));
app.use(express.static(connPath));
app.set("view engine", 'hbs');
app.set("views", templatePath);
hbs.registerPartials(partialPath)
app.use(UserRouter);
app.use(taskRouter);

app.get("*",async (req, res)=>{
    try {
        res.status(404).render('404');
    } catch (error) {
        console.log(error.message);
    }
})

app.listen(PORT,HOST,()=>{
    console.log(`Server is up on ${HOST}:${PORT}`)
})