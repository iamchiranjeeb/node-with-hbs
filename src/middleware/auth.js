const jwt = require('jsonwebtoken')
const Register = require('../models/User')

const auth = async (req, res, next) =>{
    try{
        const token = req.cookies.jwt;
        const verifyUser = jwt.verify(token,process.env.JWT_SECRET);
        console.log(verifyUser);
        const user = await Register.findOne({_id:verifyUser._id});
        console.log(user.email);
        req.token = token;
        req.user = user;
        next();
    }catch(err){
        res.status(401).send(err.message)
    }
}

module.exports = auth;