const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const validator = require('validator');
const myTasks = require('./task');

const UserSchema = new mongoose.Schema({
    fullName:{
        type: String,
        required: true
    },
    lastName:{
        type: String,
        required: true
    },
    email:{
        type: String,
        required: true,
        unique: true,
        trim:true,
        validate(val){
            if(!validator.isEmail(val)){
                throw new Error("Not a Valid Email");
            }
        }
    },
    gender:{
        type: String,
        required: true,
    },
    password:{
        type: String,
        required: true,
    },
    confirmPassword:{
        type: String,
        required: true,
    },
    //Here tokens is array of an object
    tokens:[{
        token:{
            type: String,
            required: true
        }
    }],
    avatar:{
        type: Buffer,
    }
})

//References Between User & Task & a Virtual
UserSchema.virtual('tasks',{
    ref:'myTasks',
    localField: '_id',
    foreignField: 'owner'
})

//Generating Token
UserSchema.methods.generateAuthToken = async function (){
    const user = this
    try{
        const genToken = jwt.sign({_id: user._id.toString()},process.env.JWT_SECRET)
        user.tokens = user.tokens.concat({token: genToken})
        await user.save()
        return genToken
    }catch(e){
        console.log(e.message)
    }
}

//Hashing the Password
UserSchema.pre("save", async function (next){
    //const passwordHash = await bcrypt.hash(password,10);
    const user = this
    if (user.isModified("password")){
        user.password = await bcrypt.hash(user.password,10);
        user.confirmPassword = await bcrypt.hash(user.confirmPassword,10);
        //user.confirmPassword = undefined;
    }
    next();
})

//Delete Task when user is removed
UserSchema.pre("remove", async function(next){
    const user = this
    await myTasks.deleteMany({owner:user._id});
    next();
})



const UserRegister = new mongoose.model("Registers",UserSchema)
module.exports = UserRegister;