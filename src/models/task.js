const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({
    taskName:{
        type:String,
        required:true,
        trim :true,
    },
    isCompleted:{
        type:String,
        required:true,
    },
    memo: {
        type: String,
        maxLength: 100
    },
    ownerName:{
        type:String,
        required:true
    },
    owner:{
        type:mongoose.Schema.Types.ObjectId,
        required:true,
        ref: 'Registers' //To create relation
    }
})

const MyTasks = mongoose.model('myTasks',taskSchema);

module.exports = MyTasks